<a href="https://www.debian.org/">
    <img src="https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/c555239ba0f80d060ed8f22ccff58789/Debian_logo.png" alt="Aimeos logo" title="Aimeos" align="right" height="60" />
</a>

Cloud Images Debian
===================

[![Build Status](https://gitlab.com/arthurbdiniz/cloud-images-debian/badges/master/build.svg)](https://gitlab.com/arthurbdiniz/cloud-images-debian/commits/master) [![Coverage Report](https://gitlab.com/arthurbdiniz/cloud-images-debian/badges/master/coverage.svg)](https://gitlab.com/arthurbdiniz/cloud-images-debian/commits/master)


A vue.js application that show all debian cloud images in a fast way.

[![Cloud Images Debian demo](https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/df30917231d3e85ea10ee3eb0bd3976f/image.png)](http://typo3.demo.aimeos.org/)

## Table of Contents
- [Introduction](#introduction)
- [Demo](#demo)
- [Docker](#docker)
- [Installation](#installation)
- [Features](#features)
- [Site](#site)
- [Mock](#mock)
- [CI-CD](#ci-cd)
  - [Continuous Integration](#continuous-integration)
  - [Continuous Deploy](#continuous-deploy)
- [Credits](#credits)


## Introduction
This application was develop to the Debian GSoC task using Vue.js framework. The main objective is to provide a site that makes easy to find  Debian Cloud Images for each provider.

## Demo
Here is a working live demo : https://cloud-images.arthurbdiniz.com

## Docker

* Install Docker
  - [Download docker](https://docs.docker.com/engine/installation/)

* Install Docker Compose
  - [Download docker-compose](https://docs.docker.com/compose/install/)

### Development Environment
For development you can open in your browser on [localhost:8080](localhost:8080)

**Run**

The following command run docker image.
```bash
sudo docker-compose -f dev.yml up
```

**Build Image**

The following command build docker image
```bash
sudo docker-compose -f dev.yml build
```

**Run/Build Image**

The following command to run and build docker
```bash
sudo docker-compose -f dev.yml up --build
```

### Production Environment
For production you can open in your browser on [localhost:5000](localhost:5000)

**Run**

Run the following command to run docker
```bash
sudo docker-compose -f prod.yml up
```

**Build Image**

Run the following command to build docker
```bash
sudo docker-compose -f prod.yml build
```

**Run/Build Image**

Run the following command to run and build docker
```bash
sudo docker-compose -f prod.yml up --build
```

## Installation

#### Recomended setup
```
npm -v
6.5.0
-------
node -v
v11.3.0
```

#### [Node](https://nodejs.org/en/download/)
#### [NPM](https://www.npmjs.com/)

## Project setup

#### Development

```bash
# Install all node dependencies
npm install

# Compiles and hot-reloads for development
npm run serve
```

#### Test & Lint

```bash
# Run your tests
npm run test

# Run your end-to-end tests
npm run test:e2e

# Run your unit tests
npm run test:unit

# Lints and fixes files
npm run lint
```

#### Production

```bash
# Compiles and minifies for production
npm run build
```

## Features
- Providers filter home
- Details Page
    - List on all debian images for specific provider with search and link to image on provider
    - About provider
    - Image usage with tips
    - Public review
    - Pipeline of images been deployed
    - Licenses
- Notification Panel
- Settings Panel
- Debian cloud CI
- Bug Report
- Forum

## Site

#### Landing Page
This page shows all debian providers that debian has image built.
![Landing Page](https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/df30917231d3e85ea10ee3eb0bd3976f/image.png)

#### Provider Page
This page is the root of link all information of that image.
![Provider Page](https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/3a5c953fa384f2aad2c40e073356ad24/image.png)

#### Image Description
This fragment will be responsible to provide the list of images with name, zone, version, code name and architecture and link to provider. And if the user wants to know more about the provider below there is a brief description.
![Image Description](https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/a6f96c498ba706ac9cf22cd86818c4a9/image.png)

![](https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/47f6ff7501bdd69e53963f1c8c90ef7c/image.png
)

#### Image Usage
This fragment have a showcase that shows how to use the image.
![Image Usage](https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/bad41e8c4106ad0d2ef2241555f2ef81/image.png
)

#### Image Reviews
This is for a future implementation but running just with a mock the idea is to show what users thing about the image of that provider.
![Image Reviews](https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/a036b69d6e6a5671bfc154e2dd00fb62/image.png
)

#### Image CI/CD
The purpose of this part is to linked with salsa show all images that are been built to that provider in real-time and create a direct link to the image.
![Image CI/CD](https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/0eef9006578b00cd62d2ce72ca7ddec1/image.png
)

#### Image Licenses
Show the licences linked to that provider.
![Image Licenses](https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/9a5eeb226ea844aca38ab165b5dd1659/image.png
)

## Mock
The mock data was introduced inside each page and fragment. For example on `Landing Page` the mock is inside `/src/views/Home.vue`.

The structure represents a list of links that each link have objects refering the cloud providers. The cloud provider have `id`, `name`, `description`, `image`, `link` and `color` atributes.
```js
data () {
  return {
    links: [
      {
        id: '1',
        title: 'Azure',
        description: 'Microsoft Azure is an open, flexible, enterprise-grade cloud computing platform',
        image: 'https://firebasestorage.googleapis.com/v0/b/cloud-images-debian.appspot.com/o/azure.png?alt=media&token=85dad646-d6d2-4b58-bae3-1f0b48578561',
        link: '/',
        color: 'orange'
      },
      {
        id: '2',
        title: 'EC2',
        description: 'Amazon Elastic Compute Cloud (Amazon EC2) is a web service that provides secure, resizable compute capacity in the cloud.',
        image: 'https://firebasestorage.googleapis.com/v0/b/cloud-images-debian.appspot.com/o/aws.png?alt=media&token=0dbfb1da-a34b-494f-8d19-3681fabef262',
        link: '/',
        color: 'teal'
      },
      {
        id: '3',
        title: 'GCE',
        description: 'Google Compute Engine delivers virtual machines running in Google`s innovative data centers and worldwide fiber network.',
        image: 'https://firebasestorage.googleapis.com/v0/b/cloud-images-debian.appspot.com/o/gce.png?alt=media&token=ab4827ff-7944-4e31-b720-95732545f428',
        link: '/',
        color: 'blue'
      },
      {
        id: '4',
        title: 'OpenStack',
        description: 'OpenStack software controls large pools of compute, storage, and networking resources throughout a datacenter, managed through a dashboard or via the OpenStack API. ',
        image: 'https://firebasestorage.googleapis.com/v0/b/cloud-images-debian.appspot.com/o/open_stack.png?alt=media&token=eb5d4c5a-1593-4f01-9d4c-7859228fcd47',
        link: '/',
        color: 'purple'
      }
    ]
  }
}
```
## CI-CD
In this section im going to explane the Continuous Integration and Continuous Deploy pipeline. The pipeline small and concise, the worker its a Gitlab CI instance which the configuration file is  `.gitlab-ci.yml`.

![Pipeline](https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/e20a1aca23f31c812fcc4424e58bf1b3/image.png)

![Pipeline2](https://gitlab.com/arthurbdiniz/cloud-images-debian/uploads/b3d20a5b2a708cbb4825d495514c86e0/image.png)

#### Continuous Integration

The Continuous Integration is responsable for three jobs. 
  1) Build
  2) Test
  3) Lint

Since the configuration file uses a `node` image, all the tasks within the jobs use `npm's` standard commands such as `npm install --progress=false` to install all dependencies, `npm run build`to compile assets production and `npm run lint` for the lint.

#### Continuous Deploy
The Continuous Deploy uses the `firebase host service` to deploy. As the firebase dissolves an `npm` packet to communicate with your services we will give the same image of the node defined at the beginning of the file. And with just 
```bash
firebase deploy -m "Pipeline $CI_PIPELINE_ID, build $CI_BUILD_ID" --non-interactive --token $FIREBASE_DEPLOY_KEY
```
passing the credentials we can do an automated deploy with almost no downtime.

#### Credentials
Looking at the above topic you should be wondering where the environment variables used in the command are saved.

All sensitive data is using the [GitLab Environment Variable](https://gitlab.com/arthurbdiniz/cloud-images-debian/settings/ci_cd) gitlab service. In this way only the project maintainers have access and writing power over these variables.


## Credits
This software uses the following open source packages:
- [Node.js](https://nodejs.org/en/download/)
- [Vuetify](https://vuetifyjs.com)
- [vue-code-highlight](https://www.npmjs.com/package/vue-code-highlight)
